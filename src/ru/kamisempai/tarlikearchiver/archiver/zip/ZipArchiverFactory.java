package ru.kamisempai.tarlikearchiver.archiver.zip;

import ru.kamisempai.tarlikearchiver.archiver.IArchiverFactory;
import ru.kamisempai.tarlikearchiver.archiver.ICompressor;
import ru.kamisempai.tarlikearchiver.archiver.IDecomperssor;


public class ZipArchiverFactory implements IArchiverFactory {

	@Override
	public ICompressor newCompressor() {
		return new ZipCompressor();
	}

	@Override
	public IDecomperssor newDecompressor() {
		return new ZipDecompressor();
	}

}
