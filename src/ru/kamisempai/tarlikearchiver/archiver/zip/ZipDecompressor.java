package ru.kamisempai.tarlikearchiver.archiver.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ru.kamisempai.tarlikearchiver.archiver.IDecomperssor;
import ru.kamisempai.tarlikearchiver.archiver.IArchiverProgressListener;
import ru.kamisempai.tarlikearchiver.utils.FileUtils;

public class ZipDecompressor implements IDecomperssor {
	private static final int BUFFER = 2048;

	@Override
	public boolean decompress(String compressedFilePath, String decompressFolderPath) {
		boolean result = true;
		FileUtils.createDir(decompressFolderPath);
		File decompressFolder = new File(decompressFolderPath);
		ZipInputStream input = null;
		try {
			FileInputStream dest = new FileInputStream(compressedFilePath);
			input = new ZipInputStream(new BufferedInputStream(dest));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		ZipEntry ze;
		BufferedOutputStream bufOut = null;
		try {
		    while ((ze = input.getNextEntry()) != null) {
		    	String fullName = decompressFolder.getAbsolutePath() + "/" + ze.getName();
		    	if(ze.isDirectory()) FileUtils.createDir(fullName);
		    	else {
		    		FileOutputStream fo = new FileOutputStream(fullName);
		    		bufOut = new BufferedOutputStream(fo);
		    		byte data[] = new byte[BUFFER];
		    		int count;
		    		while ((count = input.read(data, 0, BUFFER)) != -1) {
						bufOut.write(data, 0, count);
					}
		    		bufOut.close();
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		try {
			if(input != null) input.close();
			if(bufOut != null) bufOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void registerProgressListener(IArchiverProgressListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterProgressListener(IArchiverProgressListener listener) {
		// TODO Auto-generated method stub
		
	}

}
