package ru.kamisempai.tarlikearchiver.archiver.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ru.kamisempai.tarlikearchiver.archiver.ICompressor;
import ru.kamisempai.tarlikearchiver.archiver.IArchiverProgressListener;

public class ZipCompressor implements ICompressor {
	private static final int BUFFER = 2048;

	@Override
	public boolean compress(List<String> files, String compressFile) {
		boolean result = true;
		ZipOutputStream out = null;
		try {
			FileOutputStream dest = new FileOutputStream(compressFile);
			out = new ZipOutputStream(new BufferedOutputStream(dest));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			result = false;
		}
		if(result) {
			for(String file: files) {
				if(!(result = compress(file, "", out)))
					break;
			}
		}
		try {
			if(out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	private boolean compress(String fileOrDir, String inerFolder, ZipOutputStream out) {
		boolean result = true;
		File fFrom = new File(fileOrDir);
		if (fFrom.isDirectory()) {
			if("".equals(inerFolder)) inerFolder = fFrom.getName() + "/";
			else inerFolder = inerFolder + fFrom.getName() + "/";
			try {
				out.putNextEntry(new ZipEntry(inerFolder));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			String[] FilesList = fFrom.list();
			for (int i = 0; i < FilesList.length; i++)
				if (!compress(fFrom.getAbsolutePath() + "/" + FilesList[i], inerFolder, out))
					return false;
		} else if (fFrom.isFile()) {
			BufferedInputStream origin = null;
			try {
				byte data[] = new byte[BUFFER];
				FileInputStream fi = new FileInputStream(fileOrDir);
				origin = new BufferedInputStream(fi, BUFFER);
				ZipEntry entry = new ZipEntry(inerFolder + fFrom.getName());
				out.putNextEntry(entry);
				int count;
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
				}
			} catch (Exception e) {
				e.printStackTrace();
				result = false;
			}
			try {
				if(origin != null) origin.close();
			} catch (IOException e) {
				e.printStackTrace();
				result = false;
			}
		}
		return result;
	}

	@Override
	public void registerProgressListener(IArchiverProgressListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterProgressListener(IArchiverProgressListener listener) {
		// TODO Auto-generated method stub
		
	}
}
