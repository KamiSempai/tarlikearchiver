package ru.kamisempai.tarlikearchiver.archiver;

/** An interface for decompress zip file*/
public interface IDecomperssor {
	/** 
	 * Decompress zip file in to decompressFolder.
	 * @param compressedFile File that will be decompresed.
	 * @param decompressFolder Folder which will contain decompressed files.
	 * @return true if decompression is success, false otherwise.
	 */
	public boolean decompress(String compressedFile, String decompressFolder);
	
	public void registerProgressListener(IArchiverProgressListener listener);
	public void unregisterProgressListener(IArchiverProgressListener listener);
}
