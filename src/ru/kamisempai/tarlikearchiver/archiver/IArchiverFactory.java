package ru.kamisempai.tarlikearchiver.archiver;

/** Factory for union compressor and decompressos that implements one archiver algorythm. */
public interface IArchiverFactory {
	
	/** Create new Compressor. */
	public ICompressor newCompressor();
	/** Create new Decompressor. */
	public IDecomperssor newDecompressor();
}
