package ru.kamisempai.tarlikearchiver.archiver;

import android.os.AsyncTask;

/** 
 * Special class for decompressing in a separate thread.
 */
public class AsyncDecompressor extends AsyncTask<Void, Float, Boolean> implements IArchiverProgressListener {

	private IDecomperssor mDecomperssor;
	private String mCompressedFile;
	private String mDecompressFolder;
	
	/**
	 * Public constructor
	 * @param decomperssor IArchiver that will be used for decompressing.
	 * @param compressedFile Compressed file that should be decompressed.
	 * @param decompressFolder Folder which will contain decompressed files.
	 */
	public AsyncDecompressor(IDecomperssor decomperssor, String compressedFile, String decompressFolder) {
		super();
		mDecomperssor = decomperssor;
		mDecomperssor.registerProgressListener(this);
		mCompressedFile = compressedFile;
		mDecompressFolder = decompressFolder;
	}
	
	@Override
	protected final Boolean doInBackground(Void... params) {
		return mDecomperssor.decompress(mCompressedFile, mDecompressFolder);
	}

	@Override
	public void onArchiverProgress(float progress) {
		publishProgress(progress);
	}

}