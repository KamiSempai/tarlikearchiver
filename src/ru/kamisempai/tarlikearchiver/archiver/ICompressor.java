package ru.kamisempai.tarlikearchiver.archiver;

import java.util.List;

/** An interface for compress list if files*/
public interface ICompressor {
	/** 
	 * Compress list of files in to compressFile
	 * @param files List of files that will be compressed.
	 * @param compressFile Result file of compression.
	 * @return true if compression is success, false otherwise.
	 */
	public boolean compress(List<String> files, String compressFile);
	
	public void registerProgressListener(IArchiverProgressListener listener);
	public void unregisterProgressListener(IArchiverProgressListener listener);
}
