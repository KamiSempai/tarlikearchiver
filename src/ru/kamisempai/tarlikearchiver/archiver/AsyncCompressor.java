package ru.kamisempai.tarlikearchiver.archiver;

import java.util.List;

import android.os.AsyncTask;

/** 
 * Special class for compressing in a separate thread.
 */
public class AsyncCompressor extends AsyncTask<Void, Float, Boolean> implements IArchiverProgressListener {

	private ICompressor mCompressor;
	private List<String> mFiles;
	private String mCompressFile;
	
	/**
	 * Public constructor
	 * @param compressor IArchiver that will be used for compressing.
	 * @param files File list for compression.
	 * @param compressFile Result file of compression.
	 */
	public AsyncCompressor(ICompressor compressor, List<String> files, String compressFile) {
		super();
		mCompressor = compressor;
		mCompressor.registerProgressListener(this);
		mFiles = files;
		mCompressFile = compressFile;
	}
	
	@Override
	protected final Boolean doInBackground(Void... params) {
		return mCompressor.compress(mFiles, mCompressFile);
	}

	@Override
	public void onArchiverProgress(float progress) {
		publishProgress(progress);
	}

}
