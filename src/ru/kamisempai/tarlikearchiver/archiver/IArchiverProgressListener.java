package ru.kamisempai.tarlikearchiver.archiver;

/** An interface of Archiver for receiving progress of compressing or decompressing. */
public interface IArchiverProgressListener {
	
	/**
	 * Progress of compressing or decompressing.
	 * @param progress Progress in percentage between 0 and 100.
	 */
	public void onArchiverProgress(float progress);
}
