package ru.kamisempai.tarlikearchiver;

import java.util.ArrayList;

import ru.kamisempai.tarlikearchiver.archiver.AsyncCompressor;
import ru.kamisempai.tarlikearchiver.archiver.AsyncDecompressor;
import ru.kamisempai.tarlikearchiver.archiver.IArchiverFactory;
import ru.kamisempai.tarlikearchiver.archiver.zip.ZipArchiverFactory;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	private IArchiverFactory mArchiverFactory;
	private EditText mEditFile;
	private EditText mEditZip;
	private EditText mEditDecompress;
	
	private Button mButtonCompress;
	private Button mButtonDecompress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mArchiverFactory = new ZipArchiverFactory();
		
		mEditFile = (EditText) findViewById(R.id.editFile);
		mEditZip = (EditText) findViewById(R.id.editZip);
		mEditDecompress = (EditText) findViewById(R.id.editDecompres);
		
		mButtonCompress = (Button) findViewById(R.id.buttonCompress);
		mButtonCompress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String file = mEditFile.getText().toString();
				String zip = mEditZip.getText().toString();
				ArrayList<String> fileList = new ArrayList<String>();
				fileList.add(file);
				AsyncCompressor compressor = new AsyncCompressor(mArchiverFactory.newCompressor(), fileList, zip) {
					@Override
					protected void onPreExecute() {
						Toast.makeText(MainActivity.this, "Start archivation...", Toast.LENGTH_SHORT).show();
					}
					
					@Override
					protected void onPostExecute(Boolean result) {
						if(result)
							Toast.makeText(MainActivity.this, "Zip archive created.", Toast.LENGTH_SHORT).show();
						else
							Toast.makeText(MainActivity.this, "Failed to create Zip archive.", Toast.LENGTH_SHORT).show();
					}
				};
				compressor.execute();
			}
		});
		
		mButtonDecompress = (Button) findViewById(R.id.buttonDecompress);
		mButtonDecompress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String zip = mEditZip.getText().toString();
				String decompressFolder = mEditDecompress.getText().toString();
				AsyncDecompressor decompressor = new AsyncDecompressor(mArchiverFactory.newDecompressor(), zip, decompressFolder) {
					@Override
					protected void onPreExecute() {
						Toast.makeText(MainActivity.this, "Start decompression...", Toast.LENGTH_SHORT).show();
					}
					
					@Override
					protected void onPostExecute(Boolean result) {
						if(result)
							Toast.makeText(MainActivity.this, "Zip archive decompressed.", Toast.LENGTH_SHORT).show();
						else
							Toast.makeText(MainActivity.this, "Failed to decompress Zip archive.", Toast.LENGTH_SHORT).show();
					}
				};
				decompressor.execute();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
