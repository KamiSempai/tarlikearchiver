package ru.kamisempai.tarlikearchiver.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.os.Environment;

public final class FileUtils {
	
	private FileUtils() {
		
	}

	/** True ���� SD ����� ����������. */
	public static boolean sdCardEnabled() {
		return Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}

	/** ���������� ���� � ����� ������. Null ���� ����� �� ����������. */
	public static String getExternalStorageDirectory() {
		if (sdCardEnabled())
			return Environment.getExternalStorageDirectory().toString();
		return null;
	}

	/** ������� �����. ���� ����� ����������, �� ���� �� ����������. */
	public static boolean createDir(String pFolderPath) {
        File folder = new File(pFolderPath);
        if (!folder.exists())
        	try {
        		folder.mkdirs();
    		} catch (Exception e) {
    			e.printStackTrace();
    			return false;
    		}
        return true;
	}
	
	/** ������� ������ ����. ���� ���� ����������, �� ���� �� ����������. */
	public static boolean createFile(String pFilePath) {
        File file = new File(pFilePath);
        if (!file.exists())
        	try {
        		return file.createNewFile();
    		} catch (Exception e) {
    			e.printStackTrace();
    			return false;
    		}
        return true;
	}
	
	public static boolean rename(String oldname, String newname) {
		File file = new File(oldname);
	    File file2 = new File(newname);
	    if(file2.exists()) return false;
	    return file.renameTo(file2);
	}
	
	public static boolean rename(File file, File file2) {
	    if(file2.exists()) return false;
	    return file.renameTo(file2);
	}
	
	/** �������� ���� ��� ����������. */
	public static boolean copy(String from, String to) {
		try {
			File fFrom = new File(from);
			if (fFrom.isDirectory()) {
				createDir(to);
				String[] FilesList = fFrom.list();
				for (int i = 0; i < FilesList.length; i++)
					if (!copy(from + "/" + FilesList[i], to + "/"
							+ FilesList[i]))
						return false; // ���� ��� ����������� ��������� ������
			} else if (fFrom.isFile()) {
				File fTo = new File(to);
				InputStream in = new FileInputStream(fFrom);
				OutputStream out = new FileOutputStream(fTo);
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/** �������� ���� ��� ����������. */
	public static boolean copy(File fFrom, File fTo) {
		try {
			if (fFrom.isDirectory()) {
				createDir(fTo.getAbsolutePath());
				String[] FilesList = fFrom.list();
				for (int i = 0; i <= FilesList.length; i++)
					if (!copy(fFrom.getAbsolutePath() + "/" + FilesList[i], fTo.getAbsolutePath() + "/"
							+ FilesList[i]))
						return false; // ���� ��� ����������� ��������� ������
			} else if (fFrom.isFile()) {
				InputStream in = new FileInputStream(fFrom);
				OutputStream out = new FileOutputStream(fTo);
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/** �������� ���� ��� ���������� � ��������� �����. */
	public static boolean copyToDir(String from, String toDir) {
		try {
			File fFrom = new File(from);
			createDir(toDir);
			if (fFrom.isDirectory()) {
				String[] FilesList = fFrom.list();
				for (int i = 0; i <= FilesList.length; i++)
					if (!copy(from + "/" + FilesList[i], toDir + "/"
							+ FilesList[i]))
						return false; // ���� ��� ����������� ��������� ������
			} else if (fFrom.isFile()) {
				File fTo = new File(toDir + "/" + fFrom.getName());
				InputStream in = new FileInputStream(fFrom);
				OutputStream out = new FileOutputStream(fTo);
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/** ������� ���� ��� ����������. */
	public static boolean delete(String path) {
		File file = new File(path);
		if (file.exists()) {
			if(file.isDirectory()) {
				String deleteCmd = "rm -r " + path;
				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec(deleteCmd);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
			else return file.delete();
		}
		
		return true;
	}
	
	/** ���������� ���������� �����. ������ � ������ � ������. */
	public static String getFileExtension(String pFileName) {
		String extension = "";

		int i = pFileName.lastIndexOf('.');
		int p = Math.max(pFileName.lastIndexOf('/'), pFileName.lastIndexOf('\\'));

		if (i > p) {
		    extension = pFileName.substring(i);
		    
		}
		return extension;
	}
	
	/** ������� ��� ����� � ����������, �� ��������� ��������.
	 *  
	 * @param directory - ���������� ������.
	 * @param filter - ������ ������
	 * @param recurse - ������������ ������� �����������. -1 ��� ���������� �����������.
	 * @return ������ ������.
	 */
	public static ArrayList<File> getFilesList(String directory, FilenameFilter filter, int recurse) {
		FilenameFilter[] filters = new FilenameFilter[1];
		filters[0] = filter;
		return getFilesList(new File(directory), filters, recurse);
	}
	
	/** ������� ��� ����� � ����������, �� ��������� ��������.
	 *  
	 * @param directory - ���������� ������.
	 * @param filters - ������� ������
	 * @param recurse - ������������ ������� �����������. -1 ��� ���������� �����������.
	 * @return ������ ������.
	 */
	public static ArrayList<File> getFilesList(File directory, FilenameFilter[] filters, int recurse) {
		ArrayList<File> files = new ArrayList<File>();
	    File[] entries = directory.listFiles();
	    if (entries != null) {
	        for (File entry : entries) {
	            for (FilenameFilter filefilter : filters) {
	                if (filters == null || filefilter.accept(directory, entry.getName())) {
	                    files.add(entry);
	                }
	            }
	            if ((recurse <= -1) || (recurse > 0 && entry.isDirectory())) {
	                recurse--;
	                files.addAll(getFilesList(entry, filters, recurse));
	                recurse++;
	            }
	        }
	    }
	    return files;
	}
}
